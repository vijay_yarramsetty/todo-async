from tortoise.contrib.pydantic import pydantic_model_creator
from .models import Post

# Pydantic Model for a serialized response to client
Post_Pydantic = pydantic_model_creator(Post, name="Post")

# Pydantic Model for parsing incoming request data into python native data types.
PostIn_Pydantic = pydantic_model_creator(Post, name="PostIn", exclude_readonly=True)
