
from fastapi import APIRouter, HTTPException
from pydantic import BaseModel
from typing import List
from tortoise.contrib.fastapi import HTTPNotFoundError
from .models import Post
from .schemas import Post_Pydantic, PostIn_Pydantic


# pydantic status model for custom messages to client
class Status(BaseModel):
    message: str


router = APIRouter()


@router.get('/')
async def read_root():
    return {"Hello": "World"}


@router.get('/posts', response_model=List[Post_Pydantic])
async def get_posts():
    return await Post_Pydantic.from_queryset(Post.all())


@router.get('/posts/{post_id}', response_model=Post_Pydantic, responses={404: {"model": HTTPNotFoundError}})
async def get_post(post_id: int):
    return await Post_Pydantic.from_queryset_single(Post.get_or_none(id=post_id))


@router.post('/posts', response_model=Post_Pydantic)
async def create_post(post: PostIn_Pydantic):
    post_obj = await Post.create(**post.dict(exclude_unset=True))
    return await Post_Pydantic.from_tortoise_orm(post_obj)


@router.put('/posts/{post_id}', response_model=Post_Pydantic, responses={404: {"model": HTTPNotFoundError}})
async def update_post(post_id: int, post: PostIn_Pydantic):
    await Post.filter(id=post_id).update(**post.dict(exclude_unset=True))
    return await Post_Pydantic.from_queryset_single(Post.get(id=post_id))


@router.delete('/posts/{post_id}', response_model=Status)
async def delete_post(post_id: int):
    obj = await Post.filter(id=post_id).first()
    if obj is None:
        raise HTTPException(status_code=404, detail=f"Post {post_id} not found")
    await obj.delete()
    return Status(message=f"Post {post_id} deleted")
