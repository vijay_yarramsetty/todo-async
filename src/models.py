from tortoise.models import Model
from tortoise import fields


# Post model built in Tortoise ORM
class Post(Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=100)
    content = fields.TextField()

    def __str__(self) -> str:
        return f"<Post(title: {self.title})>"
