from fastapi import FastAPI
import uvicorn
from tortoise.contrib.fastapi import register_tortoise
from src.routes import router

# instantiating fastapi application
app = FastAPI()

# adding all CRUD routes to the application instance
app.include_router(router)

# establishing connection to database
register_tortoise(
    app,
    db_url="sqlite://db.sqlite3",
    # db_url="postgres://vijay:test@localhost/vijay",
    modules={"models": ["src.models"]},
    generate_schemas=True,
    add_exception_handlers=True,
)

if __name__ == "__main__":
    uvicorn.run("main:app", port=8000, reload=True)
