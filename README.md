### Setup:
* Create a virtual environment for this FastAPI application
* Install the dependencies from requirements.txt using `pip install -r requirements.txt`
* To run the web server **Uvicorn** you can either run main.py file from CLI or use the go to the project root directory from CLI and use the command `uvicorn main:app --reload` 

### Techstack:
* FastAPI Python Framework
* Tortoise ORM for connecting to databases.
* **aiosqlite** db driver for connecting to sqlite database from tortoise orm. 
* **asyncpg** db driver for connecting to postgres database from tortoise orm.